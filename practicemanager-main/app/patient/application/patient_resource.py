from flask import request, jsonify, Blueprint
from flask import current_app as app

from patient.domain.patient import Patient

from patient.infrastructure.patient_entity import PatientEntity

from flask import abort 
import json
from flask import Flask
from flask_oidc import OpenIDConnect
import jwt



patient_bp = Blueprint('patient', __name__)

oidc_patient = OpenIDConnect()


@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients', methods=['GET'])
def get_all_patients():
    try:
        patient_service = app.config['patient_service']
        patients = patient_service.get_all_patients()
        return jsonify([patient.to_dict() for patient in patients]), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_id(patient_id)
        if patient:
            return jsonify(patient.to_dict()), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.delete_patient(patient_id)
        if patient:
            return jsonify({'message': 'Patient deleted successfully'}), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        patient = patient_service.update_patient(patient_id, patient_data)
        if patient:
            return jsonify(patient.to_dict()), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
    
    

@patient_bp.route('/patients', methods=['GET'])
def get_patient_by_name():
    try:
        name = request.args.get('name')
        if not name:
            return jsonify({'error': 'Name parameter is required'}), 400
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_name(name)
    except Exception as e:
        return jsonify({'error': str(e)}), 400



@patient_bp.route('/patients', methods=['GET'])
def get_patient_by_ssn():
    try:
        ssn = request.args.get('ssn')
        if not ssn:
            return jsonify({'error': 'SSN parameter is required'}), 400
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_ssn(ssn)
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
    
#For the doctor


@patient_bp.route('/doctor', methods=['GET'])
@oidc_patient.accept_token()

def get_patients():
        """OAuth 2.0 protected API endpoint accessible via AccessToken"""
        token = request.headers.get('Authorization').split(' ')[1]
        claim = jwt.get_unverified_claims(token)
        roles = claim.get('realm_access', {}).get('roles', [])
        if 'doctor' not in roles:
            abort(403, 'Access forbidden. Only users in the "doctor" group can access this endpoint.')
        patient_service = app.config['patient_service']
        patients = patient_service.get_patients()
            
       
        return json.dumps(patients)

@patient_bp.route('/doctor', methods=['POST'])
@oidc_patient.accept_token()
def create_patientdoc():
        """OAuth 2.0 protected API endpoint accessible via AccessToken"""
        token = request.headers.get('Authorization').split(' ')[1]
        claim = jwt.get_unverified_claims(token)
        roles = claim.get('realm_access', {}).get('roles', [])
        if 'doctor' not in roles:
            abort(403, 'Access forbidden. Only users in the "doctor" group can access this endpoint.')
        patient_data = request.get_json()

        new_patient = PatientEntity(
        first_name=patient_data.get('first_name'),
        last_name=patient_data.get('last_name'),
        date_of_birth=patient_data.get('date_of_birth'),
        social_security_number=patient_data.get('social_security_number'),
        id=1

    )

        patient_service = app.config['patient_service']
        patients = patient_service.create_patientdoc(new_patient)
        
        return json.dumps(patients)

        



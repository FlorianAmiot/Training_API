from patient.domain.patient import Patient

class PatientService:
    def __init__(self, patient_repository):
        self.patient_repository = patient_repository

    def create_patient(self, patient_data):
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']
        if any(not patient_data.get(field) for field in required_fields):
            raise ValueError('Missing required fields')
        created_patient = self.patient_repository.add_patient(patient_data)
        return created_patient
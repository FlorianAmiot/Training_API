from flask import Flask, Blueprint

from patient.application.patient_resource import patient_bp
from patient.application.patient_service import PatientService
from patient.infrastructure.patient_repository import PatientRepository


def create_app():
    application = Flask(__name__)

    # Configuration settings, routes, middleware, etc. can be added here
    patient_repository = PatientRepository()
    patient_service = PatientService(patient_repository)
    application.config['patient_service'] = patient_service

    application.register_blueprint(patient_bp)

    return application

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
